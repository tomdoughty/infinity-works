import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history';
import createRootReducer from './reducers';

export const history = createBrowserHistory();

const middleware = applyMiddleware(
  routerMiddleware(history),
  thunk,
);

export default () => createStore(
  createRootReducer(history),
  {},
  compose(middleware),
);
