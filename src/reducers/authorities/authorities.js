import actionTypes from '../../constants/actionTypes';

export const initialState = {
  loading: false,
  authorities: [],
};

export const authorities = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTHORITIES_BEGIN:
      return {
        ...state,
        loading: true,
      };

    case actionTypes.AUTHORITIES_SUCCESS:
      return {
        ...state,
        loading: false,
        authorities: action.body.authorities,
      };

    default:
      return state;
  }
};
