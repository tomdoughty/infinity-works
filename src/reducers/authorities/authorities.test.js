import { initialState, authorities } from './authorities';
import actionTypes from '../../constants/actionTypes';

describe('Authorities reducer', () => {
  it('should return the initial state', () => {
    expect(authorities(undefined, {})).toEqual(
      initialState,
    );
  });

  it('should handle AUTHORITIES_BEGIN', () => {
    expect(
      authorities(
        initialState,
        { type: actionTypes.AUTHORITIES_BEGIN },
      ),
    ).toEqual(
      {
        loading: true,
        authorities: [],
      },
    );
  });

  it('should handle AUTHORITIES_SUCCESS', () => {
    expect(
      authorities(
        {
          loading: true,
          authorities: [],
        },
        {
          type: actionTypes.AUTHORITIES_SUCCESS,
          body: {
            authorities: [
              { name: 'example' },
            ],
          },
        },
      ),
    ).toEqual(
      {
        loading: false,
        authorities: [
          { name: 'example' },
        ],
      },
    );
  });
});
