import actionTypes from '../../constants/actionTypes';

export const initialState = {
  error: null,
};

export default function reduce(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_ERROR:
      return { ...state, error: action.error };

    default:
      return state;
  }
}
