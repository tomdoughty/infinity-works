import error, { initialState } from './error';
import actionTypes from '../../constants/actionTypes';

describe('Error reducer', () => {
  it('should return the initial state', () => {
    expect(error(undefined, {})).toEqual(
      initialState,
    );
  });

  it('should handle SET_ERROR', () => {
    expect(
      error(
        initialState,
        {
          type: actionTypes.SET_ERROR,
          error: 'Default error message',
        },
      ),
    ).toEqual(
      {
        error: 'Default error message',
      },
    );
  });
});
