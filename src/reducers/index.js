import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { authorities } from './authorities/authorities';
import { authority } from './authority/authority';
import error from './error/error';

const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  authorities,
  authority,
  error,
});

export default createRootReducer;
