import { initialState, authority } from './authority';
import actionTypes from '../../constants/actionTypes';

describe('Authority reducer', () => {
  it('should return the initial state', () => {
    expect(authority(undefined, {})).toEqual(
      initialState,
    );
  });

  it('should handle AUTHORITY_BEGIN', () => {
    expect(authority(
      initialState,
      { type: actionTypes.AUTHORITY_BEGIN },
    )).toEqual({
      ...initialState,
      loading: true,
    });
  });

  it('should handle AUTHORITY_SUCCESS', () => {
    expect(authority({
      loading: true,
      results: [],
    },
    {
      type: actionTypes.AUTHORITY_SUCCESS,
      body: {
        establishments: [
          {
            LocalAuthorityName: 'Example Name',
            RatingValue: '5',
          },
          { RatingValue: '5' },
          { RatingValue: '1' },
          { RatingValue: 'Exempt' },
        ],
      },
    })).toEqual({
      loading: false,
      name: 'Example Name',
      results: [
        {
          rating: '5-star',
          percentage: '50',
        },
        {
          rating: '4-star',
          percentage: '0',
        },
        {
          rating: '3-star',
          percentage: '0',
        },
        {
          rating: '2-star',
          percentage: '0',
        },
        {
          rating: '1-star',
          percentage: '25',
        },
        {
          rating: 'Exempt',
          percentage: '25',
        },
      ],
    });
  });
});
