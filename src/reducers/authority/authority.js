import actionTypes from '../../constants/actionTypes';

export const initialState = {
  loading: false,
  name: null,
  results: [],
};

export const authority = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTHORITY_BEGIN:
      return {
        ...state,
        loading: true,
      };

    case actionTypes.AUTHORITY_SUCCESS: {
      const ratings = action.body.establishments.map((est) => est.RatingValue);
      const uniqueRatings = [...new Set(['5', '4', '3', '2', '1', ...ratings])];

      const results = uniqueRatings.map((uniqueRating) => {
        const occurances = ratings.filter((rating) => rating === uniqueRating).length;
        return {
          rating: uniqueRating.match(/^[0-9]+$/) ? `${uniqueRating}-star` : uniqueRating.split(/(?=[A-Z])/).join(' '),
          percentage: ((occurances / ratings.length) * 100).toFixed(0),
        };
      });

      return {
        ...state,
        loading: false,
        name: action.body.establishments[0].LocalAuthorityName,
        results,
      };
    }

    default:
      return state;
  }
};
