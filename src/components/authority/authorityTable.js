import React from 'react';
import PropTypes from 'prop-types';

const AuthorityTable = ({ results }) => (
    <table>
        <thead>
            <tr>
                <th>Rating</th>
                <th>Percentage</th>
            </tr>
        </thead>
        <tbody>
            { results.map(({ rating, percentage }, index) => (
                <tr key={`result${index}`}>
                    <td>{rating}</td>
                    <td>{percentage}%</td>
                </tr>
            ))}
        </tbody>
    </table>
);

AuthorityTable.propTypes = {
  results: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default AuthorityTable;
