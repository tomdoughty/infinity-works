import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Authority from './authority';

Enzyme.configure({ adapter: new Adapter() });

function setup() {
  const props = {
    match: { params: { id: 44 } },
    fetchAuthority: jest.fn(),
    loading: false,
    name: 'Example Name',
    results: [
      {
        rating: '5-star',
        percentage: '50',
      },
      {
        rating: '4-star',
        percentage: '0',
      },
      {
        rating: '3-star',
        percentage: '0',
      },
      {
        rating: '2-star',
        percentage: '0',
      },
      {
        rating: '1-star',
        percentage: '25',
      },
      {
        rating: 'Exempt',
        percentage: '25',
      },
    ],
  };

  const enzymeWrapper = mount(<Authority {...props} />);

  return {
    enzymeWrapper,
  };
}

describe('components', () => {
  describe('Authority', () => {
    it('should render without crashing', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.find('h1').text()).toBe('Results for: Example Name');
      expect(enzymeWrapper.find('a').text()).toBe('Back to search');
    });
  });
});
