import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import AuthorityTable from './authorityTable';

const Authority = ({
  match,
  loading,
  name,
  results,
  fetchAuthority,
}) => {
  useEffect(() => {
    (() => fetchAuthority(match.params.id))();
  }, [fetchAuthority, match.params.id]);

  if (loading) return <div className="loading">Loading</div>;

  return (<>
        <h1>Results for: { name }</h1>
        <AuthorityTable results={ results } />
        <a href="/">Back to search</a>
  </>);
};

Authority.propTypes = {
  // Injected via Redux
  match: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  results: PropTypes.arrayOf(PropTypes.object).isRequired,
  fetchAuthority: PropTypes.func.isRequired,
};

export default Authority;
