import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import AuthoritiesSearch from './authoritiesSearch';
import AuthoritiesList from './authoritiesList';

const Authorities = ({ loading, authorities, fetchAuthorities }) => {
  const [filteredAuthorities, setFilteredAuthorities] = useState([]);
  useEffect(() => {
    (() => fetchAuthorities())();
  }, [fetchAuthorities]);

  const handleSearch = (value) => {
    const lowerVal = value.toLowerCase();
    const newAuthorities = authorities.filter(({ Name }) => Name.toLowerCase().includes(lowerVal));
    return setFilteredAuthorities(newAuthorities);
  };

  if (loading) return <div className="loading">Loading</div>;

  return (<>
        <h1>Food hygiene checker</h1>
        <AuthoritiesSearch
            onChange={ handleSearch }
        />
        <AuthoritiesList
            authorities={ filteredAuthorities }
        />
  </>);
};

Authorities.propTypes = {
  // Injected via Redux
  loading: PropTypes.bool.isRequired,
  authorities: PropTypes.array.isRequired,
  fetchAuthorities: PropTypes.func.isRequired,
};

export default Authorities;
