import React from 'react';
import PropTypes from 'prop-types';

const AuthoritiesList = ({ authorities }) => (
    <ul>
        { authorities.map(({ LocalAuthorityId, Name }, index) => (
            <li key={`auth${index}`}>
                <a href={`/authority/${LocalAuthorityId}`}>{ Name }</a>
            </li>
        ))}
    </ul>
);

export default AuthoritiesList;

AuthoritiesList.propTypes = {
  authorities: PropTypes.arrayOf(PropTypes.object).isRequired,
};
