import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Authorities from './authorities';

Enzyme.configure({ adapter: new Adapter() });

function setup() {
  const props = {
    fetchAuthorities: jest.fn(),
    loading: false,
    authorities: [],
  };

  const enzymeWrapper = mount(<Authorities {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('components', () => {
  describe('Authorities', () => {
    it('should render without crashing', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.find('h1').text()).toBe('Food hygiene checker');
    });

    it('should call getAuthorities when mounted', () => {
      const { props } = setup();
      expect(props.fetchAuthorities.mock.calls.length).toBe(1);
    });
  });
});
