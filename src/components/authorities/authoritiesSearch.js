import React from 'react';
import PropTypes from 'prop-types';

const AuthoritiesSearch = ({ onChange }) => (
  <>
        <label htmlFor="authority-search">
            Search for a local authority:
        </label>
        <input
            id="authority-search"
            onChange={(e) => onChange(e.target.value)}
            placeholder="eg. Leeds"
        />
  </>
);

export default AuthoritiesSearch;

AuthoritiesSearch.propTypes = {
  onChange: PropTypes.func.isRequired,
};
