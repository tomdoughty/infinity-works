import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Error from './error';

Enzyme.configure({ adapter: new Adapter() });

function setup() {
  const props = {
    error: 'Error message',
  };

  const enzymeWrapper = mount(<Error {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('components', () => {
  describe('Error', () => {
    it('should render without crashing', () => {
      const { enzymeWrapper } = setup();
      expect(enzymeWrapper.find('a').text()).toBe('Back to homepage');
    });

    it('should render props.error if truthy', () => {
      const { enzymeWrapper, props } = setup();
      expect(enzymeWrapper.find('h1').text()).toBe('Error');
      expect(enzymeWrapper.find('p').text()).toBe(props.error);
    });

    it('should render 404 title if props.error is falsey', () => {
      const { enzymeWrapper } = setup();
      enzymeWrapper.setProps({ error: null });
      expect(enzymeWrapper.find('h1').text()).toBe('Error: 404');
      expect(enzymeWrapper.find('p').text()).toBe('Sorry we cannot find the page you are looking for');
    });
  });
});
