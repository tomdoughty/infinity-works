import React from 'react';
import PropTypes from 'prop-types';

const ErrorPage = ({ error }) => (
    <div>
        <h1>
        Error
            {error ? null : ': 404'}
        </h1>
        <p>
            {error || 'Sorry we cannot find the page you are looking for'}
        </p>
        <a href="/">Back to homepage</a>
    </div>
);

export default ErrorPage;

ErrorPage.propTypes = {
  error: PropTypes.string,
};
