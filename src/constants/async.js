export const apiBaseUrl = 'http://api.ratings.food.gov.uk';

export const headers = {
  'x-api-version': 2,
  accept: 'application/json',
};
