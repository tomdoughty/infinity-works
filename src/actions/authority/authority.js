import 'isomorphic-fetch';
import { push } from 'connected-react-router';

import fetchError from '../error/error';
import actionTypes from '../../constants/actionTypes';
import { apiBaseUrl, headers } from '../../constants/async';

const fetchAuthorityRequest = () => ({
  type: actionTypes.AUTHORITY_BEGIN,
});

const fetchAuthoritySuccess = (body) => ({
  type: actionTypes.AUTHORITY_SUCCESS,
  body,
});

export default (id) => async (dispatch) => {
  try {
    dispatch(fetchAuthorityRequest());
    const result = await fetch(`${apiBaseUrl}/Establishments/?localAuthorityId=${id}&pageSize=0`, {
      method: 'GET',
      headers,
    });
    const json = await result.json();
    dispatch(fetchAuthoritySuccess(json));
  } catch (error) {
    dispatch(fetchError(error));
    dispatch(push('/error'));
  }
};
