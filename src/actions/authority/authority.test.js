import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import 'isomorphic-fetch';

import fetchAuthority from './authority';
import actionTypes from '../../constants/actionTypes';
import { apiBaseUrl } from '../../constants/async';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const mockResult = {
  establishments: [
    {
      LocalAuthorityName: 'Example Name',
      RatingValue: '5',
    },
    { RatingValue: '5' },
    { RatingValue: '1' },
    { RatingValue: 'Exempt' },
  ],
};

const mockLocalAuthorityId = 44;

describe('Authority async actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('creates FETCH_AUTHORITY_SUCCESS when fetching authority is complete', async () => {
    fetchMock.get(`${apiBaseUrl}/Establishments/?localAuthorityId=${mockLocalAuthorityId}&pageSize=0`, { body: mockResult });

    const expectedActions = [
      { type: actionTypes.AUTHORITY_BEGIN },
      { type: actionTypes.AUTHORITY_SUCCESS, body: mockResult },
    ];

    const store = mockStore();

    await store.dispatch(fetchAuthority(mockLocalAuthorityId));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
