import actionTypes from '../../constants/actionTypes';

export default (error) => ({
  type: actionTypes.SET_ERROR,
  error,
});
