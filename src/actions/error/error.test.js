import fetchError from './error';
import actionTypes from '../../constants/actionTypes';

const error = 'Error message';

describe('Error action', () => {
  it('should create an action to add an error', () => {
    const expectedAction = {
      type: actionTypes.SET_ERROR,
      error,
    };
    expect(fetchError(error)).toEqual(expectedAction);
  });
});
