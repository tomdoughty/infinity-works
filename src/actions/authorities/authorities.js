import 'isomorphic-fetch';
import { push } from 'connected-react-router';

import fetchError from '../error/error';
import actionTypes from '../../constants/actionTypes';
import { apiBaseUrl, headers } from '../../constants/async';

const fetchAuthoritiesRequest = () => ({
  type: actionTypes.AUTHORITIES_BEGIN,
});

const fetchAuthoritiesSuccess = (body) => ({
  type: actionTypes.AUTHORITIES_SUCCESS,
  body,
});

export default () => async (dispatch) => {
  try {
    dispatch(fetchAuthoritiesRequest());
    const result = await fetch(`${apiBaseUrl}/Authorities/basic`, {
      method: 'GET',
      headers,
    });
    const json = await result.json();
    dispatch(fetchAuthoritiesSuccess(json));
  } catch (error) {
    dispatch(fetchError(error));
    dispatch(push('/error'));
  }
};
