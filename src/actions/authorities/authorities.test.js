import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import 'isomorphic-fetch';

import fetchAuthorities from './authorities';
import actionTypes from '../../constants/actionTypes';
import { apiBaseUrl } from '../../constants/async';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const mockResult = {
  establishments: [
    {
      LocalAuthorityName: 'Example Name',
      RatingValue: '5',
    },
  ],
};

describe('Authorities async actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('creates AUTHORITIES_SUCCESS when fetching authority is complete', () => {
    fetchMock.get(`${apiBaseUrl}/Authorities/basic`, { body: mockResult });

    const expectedActions = [
      { type: actionTypes.AUTHORITIES_BEGIN },
      { type: actionTypes.AUTHORITIES_SUCCESS, body: mockResult },
    ];

    const store = mockStore();

    return store.dispatch(fetchAuthorities())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });
});
