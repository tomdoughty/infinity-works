import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import configureStore, { history } from './store';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

import Authorities from './containers/authorities';
import Authority from './containers/authority';
import ErrorPage from './containers/error';

const store = configureStore();

render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <main>
                <Switch>
                    <Route path="/" exact component={Authorities} />
                    <Route path="/authority/:id" component={Authority} />
                    <Route component={ErrorPage} />
                </Switch>
            </main>
        </ConnectedRouter>
    </Provider>,
    document.querySelector('#root'),
);

registerServiceWorker();
