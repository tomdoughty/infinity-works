import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import fetchAuthority from '../actions/authority/authority';
import Authority from '../components/authority/authority';

const mapStateToProps = (state) => ({
  loading: state.authority.loading,
  name: state.authority.name,
  results: state.authority.results,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  fetchAuthority,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Authority);
