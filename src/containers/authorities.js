import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import fetchAuthorities from '../actions/authorities/authorities';
import Authorities from '../components/authorities/authorities';

const mapStateToProps = (state) => ({
  loading: state.authorities.loading,
  authorities: state.authorities.authorities,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  fetchAuthorities,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Authorities);
