import { connect } from 'react-redux';
import ErrorPage from '../components/error/error';

const mapStateToProps = (state) => ({
  error: state.error.error,
});

export default connect(
  mapStateToProps,
)(ErrorPage);
